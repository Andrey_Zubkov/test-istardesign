<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Contact;
use app\models\UserPhone;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

$this->title = 'View Contact '.$id;
$this->params['breadcrumbs'][] = $this->title;

$model = Contact::findOne($id);
?>

<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
 	<p>First name: <?=$model->first_name;?> </p>
 	<p>Last name: <?=$model->last_name;?> </p>
 	<p>Patronymic name: <?=$model->patronymic;?> </p>

 	<?= Html::a('Update', ['contact/update', 'id' => $id], ['class' => 'profile-link']) ?>

<?php 
$dataProvider = new ActiveDataProvider([
    'query' => UserPhone::find()->where(['contact_id' => $id]),
    'pagination' => [
        'pageSize' => 20,
    ],
]);

echo GridView::widget([
            'dataProvider'      => $dataProvider,
            'filterRowOptions'  => ['class' => 'hidden-xs hidden-sm'],
            'columns'       => [
            // phone number
                [
                    'attribute'         => 'phone_number',
                    'format'            => 'raw',
                    'value'             => function(UserPhone $userPhone) {
                    	return Html::tag('span', $userPhone->phone_number, ['class' => 'name']);
                    },
                ],          
            // edit
                [
                    'attribute'         => '',
                    'format'            => 'raw',
                    'value'             => function(UserPhone $userPhone) {                    	
                    	return Html::a('Edit', ['user-phone/update', 'id' => $userPhone->id, 'contact_id' => $userPhone->contact_id], ['class' => 'profile-link']);
                    },
                ],
            // delete
                [
                    'attribute'         => '',
                    'format'            => 'raw',
                    'value'             => function(UserPhone $userPhone) {                    	
                    	return Html::a('Delete', ['user-phone/delete', 'id' => $userPhone->id], ['class' => 'profile-link']);
                    },
                ],
            ]
        ]);
?>

 	<?= Html::a('Add phone', ['/user-phone/create', 'contact_id' => $id], ['class' => 'profile-link']) ?>

</div>