<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Contact;
use app\models\UserPhone;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;


$this->title = 'Update Contact';
$this->params['breadcrumbs'][] = $this->title;

$model = Contact::findOne($id);
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
    ]); ?>

        <?= $form->field($model, 'first_name')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'last_name')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'patronymic')->textInput(['autofocus' => true]) ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
    <?php 
        $dataProvider = new ActiveDataProvider([
            'query' => UserPhone::find()->where(['contact_id' => $id]),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        echo GridView::widget([
                    'dataProvider'      => $dataProvider,
                    'filterRowOptions'  => ['class' => 'hidden-xs hidden-sm'],
                    'columns'       => [
                    // phone number
                        [
                            'attribute'         => 'phone_number',
                            'format'            => 'raw',
                            'value'             => function(UserPhone $userPhone) {
                                return Html::tag('span', $userPhone->phone_number, ['class' => 'name']);
                            },
                        ],          
                    // edit
                        [
                            'attribute'         => '',
                            'format'            => 'raw',
                            'value'             => function(UserPhone $userPhone) {                     
                                return Html::a('Edit', ['user-phone/update', 'id' => $userPhone->id, 'contact_id' => $userPhone->contact_id], ['class' => 'profile-link']);
                            },
                        ],
                    // delete
                        [
                            'attribute'         => '',
                            'format'            => 'raw',
                            'value'             => function(UserPhone $userPhone) {                     
                                return Html::a('Delete', ['user-phone/delete', 'id' => $userPhone->id], ['class' => 'profile-link']);
                            },
                        ],
                    ]
                ]);
    ?>

    <?= Html::a('Add phone', ['/user-phone/create', 'contact_id' => $id], ['class' => 'profile-link']) ?>
   
</div>