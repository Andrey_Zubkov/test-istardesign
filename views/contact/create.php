<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Contact;

$this->title = 'Create Contact';
$this->params['breadcrumbs'][] = $this->title;

$model = \Yii::createObject(['class' => Contact::className()]);
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
    ]); ?>

        <?= $form->field($model, 'first_name')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'last_name')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'patronymic')->textInput(['autofocus' => true]) ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
   
</div>