<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Contact;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

$this->title = 'Contacts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
<?php 
$dataProvider = new ActiveDataProvider([
    'query' => Contact::find(),
    'pagination' => [
        'pageSize' => 20,
    ],
]);

echo GridView::widget([
            'dataProvider'      => $dataProvider,
            'filterRowOptions'  => ['class' => 'hidden-xs hidden-sm'],
            'columns'       => [
            // first name
                [
                    'attribute'         => 'first_name',
                    'format'            => 'raw',
                    'value'             => function(Contact $contact) {
                    	return Html::tag('span', $contact->first_name, ['class' => 'name']);
                    },
                ],
            // last name
                [
                    'attribute'         => 'last_name',
                    'format'            => 'raw',
                    'value'             => function(Contact $contact) {
                    	$last_name = $contact->last_name ? $contact->last_name : 'not set';
                    	return Html::tag('span', $last_name, ['class' => 'name']);
                    },
                ],
            // patronymic
                [
                    'attribute'         => 'patronymic',
                    'format'            => 'raw',
                    'value'             => function(Contact $contact) {
                    	$patronymic = $contact->patronymic ? $contact->patronymic : 'not set';
                    	return Html::tag('span', $patronymic, ['class' => 'name']);
                    },
                ],
                // phones
                [
                    'attribute'         => 'phones',
                    'format'            => 'raw',
                    'value'             => function(Contact $contact) {
                        $phones = '';
                        if (!empty($contact->phones)) {
                            foreach ($contact->phones as $phone) {
                                 $phones .= Html::tag('span', $phone->phone_number, ['class' => 'name']).'<br>';
                            }                           
                        } else {
                            $phones = 'not set';
                        }
                        return Html::tag('span', $phones, ['class' => 'name']);
                    },
                ],
            // created_at
                [
                    'attribute'         => 'created_at',
                    'format'            => 'raw',
                    'value'             => function(Contact $contact) {                    	
                    	return Html::tag('span', date("m.d.y", $contact->created_at), ['class' => 'name']);
                    },
                ],
            // updated_at
                [
                    'attribute'         => 'updated_at',
                    'format'            => 'raw',
                    'value'             => function(Contact $contact) {                    	
                    	return Html::tag('span', date("m.d.y", $contact->updated_at), ['class' => 'name']);
                    },
                ],
            // edit
                [
                    'attribute'         => '',
                    'format'            => 'raw',
                    'value'             => function(Contact $contact) {                    	
                    	return Html::a('Edit', ['contact/update', 'id' => $contact->id], ['class' => 'profile-link']);
                    },
                ],
            // view
                [
                    'attribute'         => '',
                    'format'            => 'raw',
                    'value'             => function(Contact $contact) {                    	
                    	return Html::a('View', ['contact/view', 'id' => $contact->id], ['class' => 'profile-link']);
                    },
                ],
            // delete
                [
                    'attribute'         => '',
                    'format'            => 'raw',
                    'value'             => function(Contact $contact) {                    	
                    	return Html::a('Delete', ['contact/delete', 'id' => $contact->id], ['class' => 'profile-link']);
                    },
                ],
            ]
        ]);
?>
</div>