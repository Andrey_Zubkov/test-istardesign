<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * UserPhone is the model for phones user.
 * @property integer $contact_id id model Contact
 * @property string $phone_number phone number user
 */
class UserPhone extends ActiveRecord
{
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // contact id rules
            ['contact_id', 'integer'],
            ['contact_id', 'exist', 'skipOnError' => true, 'targetClass' => Contact::className(), 'targetAttribute' => ['contact_id' => 'id']],

            // phone number rules
            ['phone_number', 'required'],
            ['phone_number', 'match', 'pattern' => '/^[\+0-9\-\(\)\s]*$/'],
            ['phone_number', 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contact::className(), ['id' => 'contact_id']);
    }

}
