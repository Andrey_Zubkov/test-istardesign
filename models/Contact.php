<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * Contact is the model for contacts.
 * @property string $first_name First name user
 * @property string $last_name Last name user
 * @property string $patronymic Patronymic user 
 */
class Contact extends ActiveRecord
{
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // first name rules
            ['first_name', 'required'],
            ['first_name', 'string', 'max' => 25],

            // last name rules
            ['last_name', 'string', 'max' => 25],

            // patronymic rules
            ['patronymic', 'string', 'max' => 25],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhones()
    {
        return $this->hasMany(UserPhone::className(), ['contact_id' => 'id']);
    }

     /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {            
        }
        parent::afterSave($insert, $changedAttributes);
    }

}
