<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Contact;
use app\models\UserPhone;
use yii\base\UnknownClassException;

class UserPhoneController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }    

    public function actionCreate($contact_id)
    {
        $contact = Contact::findOne($contact_id);
        if (!$contact) {
            throw new UnknownClassException('Contact with '.$contact_id.' not found.');
        }

        if (count($contact->phones) >= 10) {
            Yii::$app->session->setFlash('error', 'Contact can have max 10 phones!');
            return $this->render('create', ['contact_id' => $contact_id]);
        }

        $model = \Yii::createObject(['class' => UserPhone::className()]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();

            Yii::$app->session->setFlash('success', 'Phone number successuly added.');

            $this->redirect(['/contact/view', 'id' => $contact_id]);
        }
        return $this->render('create', ['contact_id' => $contact_id]);
    }    

    public function actionUpdate($id, $contact_id)
    {
        $model = UserPhone::findOne($id);

        if (!$model) {
            throw new UnknownClassException('Contact with '.$id.' not found.');
        } else {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Phone successuly updated');
                } else {
                Yii::$app->session->setFlash('error', 'Phone failure update');
            }
            } 
        }

        return $this->render('update', ['id' => $id, 'contact_id' => $contact_id]); 
    }

    public function actionDelete($id)
    {
        $model = UserPhone::findOne($id);

        if (!$model) {
            throw new UnknownClassException('Phone with '.$id.' not found.');
        } else {
            if ($model->delete()) {               
                Yii::$app->session->setFlash('success', 'Phone successuly deleted');
            } else {
                Yii::$app->session->setFlash('error', 'Phone failure delete');
            }
        }

        $this->redirect(['/contact/index']);
    }

   
}
