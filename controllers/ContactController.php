<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Contact;
use app\models\UserPhone;
use yii\base\UnknownClassException;

class ContactController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreate()
    {
        $model = \Yii::createObject(['class' => Contact::className()]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            $this->redirect(['/contact/view', 'id' => $model->id]);
        }
        return $this->render('create');
    }

    public function actionView($id)
    {
        $model = Contact::findOne($id);

        if (!$model) {
            throw new UnknownClassException('Contact with '.$id.' not found.');
        }

        return $this->render('view', ['id' => $id]); 
    }

    public function actionUpdate($id)
    {
        $model = Contact::findOne($id);

        if (!$model) {
            throw new UnknownClassException('Contact with '.$id.' not found.');
        } else {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Contact successuly updated');
                } else {
                    Yii::$app->session->setFlash('error', 'Contact failure update');
            }
        }        
        }
        return $this->render('update', ['id' => $id]); 
    }

    public function actionDelete($id)
    {
        $model = Contact::findOne($id);

        if (!$model) {
            throw new UnknownClassException('Contact with '.$id.' not found.');
        } else {
            if ($model->delete()) {               
                Yii::$app->session->setFlash('success', 'Contact successuly deleted');
            } else {
                Yii::$app->session->setFlash('error', 'Contact failure delete');
            }
        }

        $this->redirect(['/contact/index']);
    }

   
}
