<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_phone`.
 */
class m180116_201320_create_user_phone_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_phone}}', [
            'id'            => $this->primaryKey(),
            'contact_id'    => $this->integer()->notNull(),
            'phone_number'  => $this->string(25)->notNull(),
            'created_at'    => $this->bigInteger(),
            'updated_at'    => $this->bigInteger()
        ], $tableOptions);

         $this->addForeignKey(
            '{{%fk-user_phone-contact_id}}',
            '{{%user_phone}}',
            'contact_id',
            '{{%contact}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            '{{%fk-user_phone-contact_id}}',
            '{{%user_phone}}'
        );

        $this->dropTable('{{%user_phone}}');
    }
}
