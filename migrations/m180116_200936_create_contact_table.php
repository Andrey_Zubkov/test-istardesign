<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contact`.
 */
class m180116_200936_create_contact_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%contact}}', [
            'id'            => $this->primaryKey(),
            'first_name'    => $this->string(255)->notNull(),
            'last_name'     => $this->string(255),
            'patronymic'    => $this->string(255),
            'created_at'    => $this->bigInteger(),
            'updated_at'    => $this->bigInteger()
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%contact}}');
    }
}
